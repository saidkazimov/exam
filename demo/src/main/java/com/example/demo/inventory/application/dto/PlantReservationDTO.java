package com.example.demo.inventory.application.dto;

import com.example.demo.common.domain.BusinessPeriod;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class PlantReservationDTO {
    @Id
    @GeneratedValue
    Long id;

    @Embedded
    BusinessPeriod schedule;

    @ManyToOne
    PlantInventoryItem plant;
}
