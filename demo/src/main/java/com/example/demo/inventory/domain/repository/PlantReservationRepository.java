package com.example.demo.inventory.domain.repository;

import com.example.demo.inventory.domain.model.PlantReservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Date;

@Repository
public interface PlantReservationRepository extends JpaRepository<PlantReservation, Long> {


    String RESERVATION = "" +
            "SELECT EXISTS(" +
            "SELECT r FROM plant_reservation r " +
            "WHERE r.plant_id = :ITEM_ID" +
            "AND (:START_DATE < r.end_date AND :END_DATE > r.start_date) " +
            ")";

    @Query(nativeQuery = true, value = RESERVATION)
    boolean getItemReservation(@Param("ITEM_ID") Long itemId, @Param("START_DATE") LocalDate startDate, @Param("END_DATE") LocalDate endDate);

    String GET_BY_PLANT_ID = "" +
            "SELECT r FROM plant_reservation r " +
            "WHERE r.plant_id = :ITEM_ID" +
            "order by r.id desc ";

    @Query(nativeQuery = true, value = GET_BY_PLANT_ID)
    PlantReservation findByPlantId(@Param("ITEM_ID") Long itemId);


}
